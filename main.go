package main

import (
	"fmt"

	"github.com/iamgoroot/wild"
)

//Define named gowild types
//Note: how about `type T _` syntax ?
type T wild.Wild
type V wild.Wild

//HolderNamed struct hoq
lding named wildtypes
type HolderNamed struct {
	Iamwild       T
	IamRegularInt int
	Iamwild2      V
	IamRegularStr string
}

func (h HolderNamed) EatMe(param T, param2 V, param3 string) {

}

func (h HolderNamed) EatMeReversed(param V, param2 T, param3 string) {

}

type _SomeHolder HolderNamed //T:string,V:int

//HolderNamed struct holding unnamed wildtypes
type HolderUnnamed struct {
	Iamwild       wild.Wild
	Iamwild2      wild.Wild
	IamRegularStr string
	IamRegularInt int
}

type _SomeHolder2 HolderUnnamed //bool, int

type TestStruct struct {
	K string
	V string
}

//HolderNamed struct holding struct and error
type _SomeHolderStruct HolderNamed //TestStruct, error

func main() {
	holder1 := SomeHolder{"hello", 1, 1, "fasd"}
	fmt.Println(holder1)

	holder2 := SomeHolder2{true, 2, "", 2}
	fmt.Println(holder2)

	holder3 := SomeHolderStruct{TestStruct{}, nil, nil, ""}
	fmt.Println(holder3)

	b := B{}

	a := A(b)
	R(a)

}

type A struct {
}

func (A) main() {

}

type B A

func R(a A) {
	fmt.Println("asdfas")
}
